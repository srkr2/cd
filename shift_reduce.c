#include <stdio.h>
#include <string.h>

void shift_reduce_parser(char *input)
{
    char stack[100] = "$"; // Initial stack with end marker
    strcat(input, "$");    // Add end marker to input

    char *input_ptr = input;

    printf("Stack\tInput\tAction\n");
    while (1)
    {
        int l = strlen(stack);
        printf("%s\t%s\t", stack, input_ptr);
        if (l == 2 && stack[0] == '$' && stack[1] == 'E' && *input_ptr == '$')
        {
            printf("String accepted!");
            break;
        }
        if (stack[l - 1] == '4')
        {
            stack[l - 1] = 'E';
            printf("Reduce using E -> 4\n");
        }
        else if (l >= 3 && stack[l - 1] == '3' && stack[l - 2] == 'E' && stack[l - 3] == '3')
        {
            stack[l - 3] = 'E'; // Pop 3, E, 3
            stack[l - 2] = '\0';
            printf("Reduce using E -> 3E3\n");
        }
        else if (l >= 3 && stack[l - 1] == '2' && stack[l - 2] == 'E' && stack[l - 3] == '2')
        {
            stack[l - 3] = 'E'; // Pop 2, E, 2
            stack[l - 2] = '\0';
            printf("Reduce using E -> 2E2\n");
        }
        else if (*input_ptr == '$')
        {
            printf("String rejected!\n");
            break;
        }
        else
        {
            strncat(stack, input_ptr, 1);
            input_ptr++;
            printf("Shift\n");
        }
    }
}

int main()
{
    char input[100] = "23432";
    shift_reduce_parser(input);
    return 0;
}