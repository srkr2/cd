#include <stdio.h>
#include <ctype.h>
#include <string.h>

int numOfProductions;
char productionSet[10][10];
char result[20];
char variables[20];
int numVariables;
int m = 0;

void computeFirst(char c)
{
    int i;
    if (!isupper(c))
    { // Terminal case
        result[m++] = c;
    }
    // Non-terminal case
    for (i = 0; i < numOfProductions; i++)
    {
        if (productionSet[i][0] == c)
        {
            if (productionSet[i][2] == '$')
            { // Production X → ε
                result[m++] = '$';
            }
            else if (islower(productionSet[i][2]))
            { // Production X → a..
                result[m++] = productionSet[i][2];
            }
            else
            { // Production X → Y
                computeFirst(productionSet[i][2]);
            }
        }
    }
}

int main()
{
    int i;
    char choice;
    char c;
    printf("How many number of productions? : ");
    scanf("%d", &numOfProductions);

    printf("Enter productions:\n");
    for (i = 0; i < numOfProductions; i++)
    {
        scanf(" %s", productionSet[i]);
    }

    printf("enter number of variables: ");
    scanf("%d", &numVariables);

    printf("Enter variables:\n");
    for (i = 0; i < numVariables; i++)
    {
        scanf(" %c", &variables[i]);
    }

    for (i = 0; i < numVariables; i++)
    {
        m = 0;
        computeFirst(variables[i]);
        result[m] = '\0';
        printf("\nFIRST(%c) = { %s }\n", variables[i], result);
    }

    return 0;
}
