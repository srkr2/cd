#include <stdio.h>
#include <stdlib.h>
// ending with 00
int main()
{
    char input[100];
    printf("Enter the string: ");
    scanf("%s", input);
    int state = 0;
    int i = 0;
    while (input[i] != '\0')
    {
        if (state == 0)
        {
            if (input[i] == '1')
                state = 0;
            else if (input[i] == '0')
            {
                if (input[i + 1] == '0' && input[i + 2] == '\0')
                {
                    state = 1;
                }
                else
                {
                    state = 0;
                }
            }
        }
        else if(state == 1 && input[i] == '0')
        {
            state = 2;
        }
        else
        {
            printf("Invalid string\n");
            exit(0);
        }
        i++;
    }
    if (state == 2)
    {
        printf("Accepted\n");
    }
    else
    {
        printf("Rejected\n");
    }
    return 0;
}