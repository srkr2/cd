%{
int n = 0;

%}

%%
"while"|"if"|"else"|"int"|"float"  { n++; printf("Keyword: %s\n", yytext); }
[a-zA-Z_][a-zA-Z0-9_]*           { n++; printf("Identifier: %s\n", yytext); }
"<="|"=="|"="|"++"|"-"|"*"|"+"   { n++; printf("Operator: %s\n", yytext); }
[(){}|,;]                        { n++; printf("Separator: %s\n", yytext); }
[0-9]+                           { n++; printf("Integer: %s\n", yytext); }
.                                { /* ignore other characters */ }
%%

int yywrap() {
    return 1;
}

int main() {
    yylex();
    printf("Total tokens: %d\n", n);
    return 0;
}