#include <stdio.h>
#include <string.h>
#include <ctype.h>

int numOfProductions, numVariables, m = 0,i=0,j=0;
char a[10][10], result[10], variables[10];
void follow(char c);
void first(char c);
int main()
{
    int i;
    
    printf("Enter the no.of productions:");
    scanf("%d", &numOfProductions);

    printf("Enter the productions(epsilon=$):\n");
    for (i = 0; i < numOfProductions; i++)
        scanf("%s", a[i]);

    printf("enter number of variables: ");
    scanf("%d", &numVariables);

    printf("Enter variables:\n");
    for (i = 0; i < numVariables; i++)
    {
        scanf(" %c", &variables[i]);
    }

    for (i = 0; i < numVariables; i++)
    {
        m = 0;
        follow(variables[i]);
        result[m] = '\0';
        printf("FOLLOW(%c) = { %s }\n", variables[i], result);
    }
}
void follow(char c)
{
    if (a[0][0] == c)
        result[m++] = '$';
    for (i = 0; i < numOfProductions; i++)
    {
        for (j = 2; j < strlen(a[i]); j++)
        {
            if (a[i][j] == c)
            {
                if (a[i][j + 1] != '\0')
                    first(a[i][j + 1]);

                if (a[i][j + 1] == '\0' && c != a[i][0])
                    follow(a[i][0]);
            }
        }
    }
}
void first(char c)
{
    int k;
    if (!isupper(c))
        result[m++] = c;
    for (k = 0; k < numOfProductions; k++)
    {
        if (a[k][0] == c)
        {
            if (a[k][2] == '$')
                follow(a[i][0]);
            else if (!isupper(a[k][2]))
                result[m++] = a[k][2];
            else
                first(a[k][2]);
        }
    }
}