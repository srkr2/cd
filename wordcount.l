%{
int word_count = 0;
%}

%%
[a-zA-Z0-9]+   { word_count++; }
.           {/* Ignore other characters */}
%%

int yywrap() {
    return 1;
}

int main() {
    yylex();
    printf("Total number of words: %d\n", word_count);
    return 0;
}

/*
flex wordcount.l
gcc lex.yy.c
./a.exe
*/